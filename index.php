<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Худякова Нелли Константиновна 181-322 № А-6</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<header>
    <div class="header-logo"></div>
    <div class="header-heading"><h1>Худякова Нелли Константиновна 181-322 № А-6</h1></div>
    <div></div>
</header>

<main>
    <?php
//        echo print_r($_POST);


    if ((isset($_POST['A']))&&(isset($_POST['B']))&&(isset($_POST['C']))){
//    if (array_key_exists('TASK', $_POST)) {

        $a = $_POST['A'];
        $b = $_POST['B'];
        $c = $_POST['C'];
        switch ($_POST['TASK']) {
            case 'triangle_area':
                $p = ($a + $b + $c) / 2;
                $result = round(pow(($p * ($p - $a) * ($p - $b) * ($p - $c)), 1/2), 2);
                break;
            case 'perimeter':
                $result = $a + $b + $c;
                break;
            case 'volume':
                $result = round(($a * $b * $c), 2);
                break;

            case 'mean':
                $result = round(($a + $b + $c) / 3, 2);
                break;
            case 'geometric_mean':
                $result = round(pow(($a + $b + $c), 1 / 3), 2);
                break;
            case 'cube_of_sum':
                $result = round(pow(($a + $b + $c), 3), 2);
                break;
        }
    }
    if (isset($result)) {

        $text_output = '<p>ФИО: ' . '<strong>' . $_POST['FIO'] . '</strong>' . ', Группа: ' . '<strong>' . $_POST['GROUP'] . '</strong>' . '</p>';

        if ($_POST['ABOUT'])
            $text_output .= '<p>Сведения о студенте: <strong>' . $_POST['ABOUT'] . '</strong>' . '</p>';

        $text_output .= '<p> Решаемая задача: <strong>';

        switch ($_POST['TASK']) {
            case 'triangle_area':
                $text_output .= 'Площадь треугольника' . '</strong>' . '</p>';
                break;
            case 'perimeter':
                $text_output .= 'Периметр треугольника' . '</strong>' . '</p>';
                break;
            case 'volume':
                $text_output .= 'Объем параллелепипеда' . '</strong>' . '</p>';
                break;

            case 'mean':
                $text_output .= 'Среднее арифметическое' . '</strong>' . '</p>';
                break;
            case 'geometric_mean':
                $text_output .= 'Среднее геометрическое' . '</strong>' . '</p>';
                break;
            case 'cube_of_sum':
                $text_output .= 'Куб суммы A+B+C' . '</strong>' . '</p>';
                break;
        }
        $text_output .= '<p>Входные данные: 
        A= <strong>' . $_POST['A'] . '</strong>, 
        B= <strong>' . $_POST['B'] . '</strong>, 
        C= <strong>' . $_POST['C'] . '</p>';
        $text_output .= '<p>Ответ тестируемого: <strong>' . $_POST['ANSWER'] . '</strong>' . '</p>';

        if (!is_nan($result)){
            $text_output .= '<p>Правильный ответ: <strong>' . $result . '</strong>' . '</p>';
            if (((float)$result === (float)$_POST['ANSWER'])&&($_POST['ANSWER'])!='')
                $text_output .= '<br><b>Отлично! ТЕСТ ПРОЙДЕН</b><br>';
            else $text_output .= '<br><b>ТЕСТ НЕ ПРОЙДЕН. ПОПРОБУЙТЕ ЕЩЕ РАЗ</b><br>';

        } else {
            $text_output .= '<p>Правильный ответ: <strong> Неверные данные. Решения нет.</strong>' . '</p>';
            $text_output .= '<br><b>Неверные данные. ПОПРОБУЙТЕ ЕЩЕ РАЗ</b><br>';
        }





        echo $text_output;
        echo '<a href="?F='.$_POST['FIO'].'&G='.$_POST['GROUP'].
            '" id="back_button">Решить тест еще раз</a>';

        if (array_key_exists('send_mail', $_POST)) {
//
//            ini_set( 'smtp_port', '465');
//            ini_set( 'SMTP', 'smtp.gmail.com');
//
//            mail(
//                $_POST['MAIL'], 'Test results',
//                str_replace('<br>', "\r\n", $text_output),
//                "From: auto@mail.ru\n" . "Content-Type: text/plain; charset=utf-8\n"
//            );
            echo '<br>Результаты были автоматически отправлены на email: ' . $_POST['MAIL'];
        }

        if($_POST['version']==='for_print'){
            echo '<script>window.print();</script>';
        }

    } else {
        echo '<form name="form" method="post" action="/php-lab6/index.php">';

        if ((array_key_exists('F', $_GET))&&(array_key_exists('G', $_GET))) {
            echo '<div>ФИО</div><div><input type="text" name="FIO" value="'.$_GET['F'].'"></div>';
            echo '<div>Группа</div><div><input type="text" name="GROUP" value="'.$_GET['G'].'"></div>';
        } else {
            echo '<div>ФИО</div><div><input type="text" name="FIO" required></div>';
            echo '<div>Группа</div><div><input type="text" name="GROUP" required></div>';
        }

        echo '<div>Значение А</div><div><input type="number" name="A" value="' . $num1= mt_rand(1,100). '" readonly></div>';
        echo '<div>Значение B</div><div><input type="number" name="B" value="' . $num2= mt_rand(1,100). '" readonly></div>';
        echo '<div>Значение C</div><div><input type="number" name="C" value="' . $num3= mt_rand(1,100). '" readonly></div>';

        echo '<div>Ваш ответ</div><div><input type="text" name="ANSWER" placeholder="Введите ответ с точностью до сотых"></div>';

        echo '<div>Немного о себе</div><textarea name="ABOUT"></textarea>';

        echo '<div>Тип задачи</div>';
        echo '<select name="TASK">';
        echo '<option value="triangle_area">Площадь треугольника</option>';
        echo '<option value="perimeter">Периметр треугольника</option>';
        echo '<option value="volume">Объем параллелепипеда</option>';

        echo '<option value="mean">Среднее арифметическое</option>';
        echo '<option value="geometric_mean">Среднее геометрическое</option>';
        echo '<option value="cube_of_sum">Куб суммы A+B+C</option>';
        echo '</select>';

        echo '<div>Отображение</div>';
        echo '<select name="version">';
        echo '<option value="for_browser">Версия для просмотра в браузере</option>';
        echo '<option value="for_print">Версия для печати</option>';
        echo '</select>';

        echo '<div class="send_mail"><input id="send-email-checkbox" type="checkbox" name="send_mail" value="send_mail">отправить результат теста по е-майл</div>';
        echo '<div id="email-label" class="email-field">Ваш email</div><div id="email-input-wrapper"><input type="email" name="MAIL" id="email-input"></div>';

        echo '<input type="submit" class="button" name="submit" value="Проверить">';

        echo '</form>';
    }
    ?>
</main>

<footer>

</footer>
<script type="application/javascript" src="script.js"></script>
</body>
</html>