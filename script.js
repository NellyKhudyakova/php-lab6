document.addEventListener('DOMContentLoaded', () => {
    const emailLabel = document.querySelector('#email-label');
    if (!emailLabel) {
        return;
    }
    const emailInputWrapper = document.querySelector('#email-input-wrapper');
    const emailInput = document.querySelector('#email-input');
    const sendEmailCheckbox = document.querySelector('#send-email-checkbox');

    function updateEmailFieldVisibility() {
        emailLabel.classList.toggle('hidden', !sendEmailCheckbox.checked);
        emailInputWrapper.classList.toggle('hidden', !sendEmailCheckbox.checked);
        emailInput.required = sendEmailCheckbox.checked;
    }

    updateEmailFieldVisibility();
    sendEmailCheckbox.addEventListener('change', updateEmailFieldVisibility);


});